import * as React from 'react';
import "@informatica/droplets-core/dist/themes/archipelago/archipelago.css";
import { Shell } from '@informatica/droplets-common';
import { Card,HeaderLevelProvider} from '@informatica/droplets-core';
import './App.css'

function App(props) {
    console.log("vikram222",props.props.data)
   // let data = props.props.data?props.props.data:null;
   let data= [
    {
      name: 'abc',
      id: '123'
    },
    {
      name: 'def',
      id: '456'
    },
    {
      name: 'ghi',
      id: '789'
    }
  ]
  const styles = {
    row: {
        display: "flex",
        marginBottom: "5px",
    },
    p: {
        margin: 0,
        color:"red"
    },
    p_first_child: {
        flexBasis: "20%",
        margin: 0,
        color:"black"
    },
};
      return (
        <div className="App">
              <Shell.Page>
              <Card>
            <HeaderLevelProvider level={3}>
                <Card.Header id="title" title="Micro Application--> 2" />
            </HeaderLevelProvider>
            <Card.Body>
              <div>
                Count is updated from other micro app
                </div>
                <div style={styles.row}>
                    <p style={styles.p_first_child}>Count:</p>
                    <p style={styles.p}>{props.props.data()}</p>
                </div>
            </Card.Body>
        </Card>
              </Shell.Page>
        </div>
      );
}

export default App;
